<?php
namespace drop;

error_reporting(E_ALL & ~E_NOTICE);

define('ns_base','drop');

require dirname(__DIR__).'/../src/.bootstrap.php';

$ws_api = new \drop_api\drop_api();
$ws_api->registry_set($registry);

$game_zone = new \cgame_zone();
$registry->set('game_zone',$game_zone);

$ws_server = new \ws_server\ws_server($ws_api);
$ws_server->run();

