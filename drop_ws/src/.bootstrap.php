<?php
namespace drop;

function autoload($class) {
  $parts = explode('\\', $class);

//  $filename = end($parts).'.php';

  $dirs = '';
  foreach ($parts as $part) {
    $dirs = $dirs.'/'.$part;
  }
  $file = core_path.'classes'.$dirs.'.php';

//  error_log('include file:'.$file);
  if (file_exists($file) == false) {
//    error_log('ERROR');
    return false;
  }
//  error_log('OK');
  include $file;
}

spl_autoload_register(ns_base.'\autoload');


//use \Exception;
// class Exception extends \Exception {}


// Константы:
define('index_constant','true');

// Узнаём путь до файлов сайта

$server_path = realpath(dirname(__DIR__) );
$core_path = realpath(dirname(__DIR__) ) .'/core/';

define ('core_path', $core_path);

// Подключаем настройки
include $server_path."/settings/settings.php";

// Создаем реестр
$registry = new \cregistry();
