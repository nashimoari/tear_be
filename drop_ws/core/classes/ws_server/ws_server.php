<?php
namespace ws_server;
if (!defined('index_constant')) die ('Not a valid entry point');
use \Exception;

class ws_server {
  private $sessions = array();
  private $connections;
  private $api;

  function __construct($api) {
    $this->api = $api;
    $this->api->ws_server_set($this);
    $this->connections = new \ws_server\connections($this);
    $this->connections->init();
  }

  public function run() {
    while (true) {
      $this->connections->data_wait();
      $this->api->news_check();
    }
  }

  public function onNewConnect($peername) {
    $this->handshake($peername);
  }


  private function handshake($peername) {
    echo("======================= handshake =============================\r\n");
    $info = array();
    echo "test1\r\n";
    $in_data = $this->connections->stream_by_name_read($peername);
    echo "test2\r\n";

    $http_headers = $this->http_parse_headers($in_data);

    $get_params_arr = $this->get_parse($http_headers[0]);
    $info['session']['session_id'] = $get_params_arr['user_guid'];
    $info['session']['peername'] = $peername;
    $info['method'] = $http_headers['method'];
    $info['uri'] = $http_headers['uri'];

    $address = explode(':', $peername);
    $info['ip'] = $address[0];
    $info['port'] = $address[1];
    $info['http_headers'] = $http_headers;

    echo "test3\r\n";
    if (empty($http_headers['Sec-WebSocket-Key'])) {
        echo "Error: absent Sec-WebSocket-Key";
        return false;
    }

    $this->sessions[$peername] = $info;
    print_r($info);

    //отправляем заголовок согласно протоколу вебсокета
    $SecWebSocketAccept = base64_encode(pack('H*', sha1($http_headers['Sec-WebSocket-Key'] . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
    $upgrade = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
        "Upgrade: websocket\r\n" .
        "Connection: Upgrade\r\n" .
        "Sec-WebSocket-Accept:$SecWebSocketAccept\r\n\r\n";
//    fwrite($connect, $upgrade);
    $data = $this->encode($upgrade);
    $this->connections->stream_data_send($peername,$data);

    return $http_headers;
}

private function encode($payload, $type = 'text', $masked = false)
{
    $frameHead = array();
    $payloadLength = strlen($payload);

    switch ($type) {
        case 'text':
            // first byte indicates FIN, Text-Frame (10000001):
            $frameHead[0] = 129;
            break;

        case 'close':
            // first byte indicates FIN, Close Frame(10001000):
            $frameHead[0] = 136;
            break;

        case 'ping':
            // first byte indicates FIN, Ping frame (10001001):
            $frameHead[0] = 137;
            break;

        case 'pong':
            // first byte indicates FIN, Pong frame (10001010):
            $frameHead[0] = 138;
            break;
    }

    // set mask and payload length (using 1, 3 or 9 bytes)
    if ($payloadLength > 65535) {
        $payloadLengthBin = str_split(sprintf('%064b', $payloadLength), 8);
        $frameHead[1] = ($masked === true) ? 255 : 127;
        for ($i = 0; $i < 8; $i++) {
            $frameHead[$i + 2] = bindec($payloadLengthBin[$i]);
        }
        // most significant bit MUST be 0
        if ($frameHead[2] > 127) {
            return array('type' => '', 'payload' => '', 'error' => 'frame too large (1004)');
        }
    } elseif ($payloadLength > 125) {
        $payloadLengthBin = str_split(sprintf('%016b', $payloadLength), 8);
        $frameHead[1] = ($masked === true) ? 254 : 126;
        $frameHead[2] = bindec($payloadLengthBin[0]);
        $frameHead[3] = bindec($payloadLengthBin[1]);
    } else {
        $frameHead[1] = ($masked === true) ? $payloadLength + 128 : $payloadLength;
    }

    // convert frame-head to string:
    foreach (array_keys($frameHead) as $i) {
        $frameHead[$i] = chr($frameHead[$i]);
    }
    if ($masked === true) {
        // generate a random mask:
        $mask = array();
        for ($i = 0; $i < 4; $i++) {
            $mask[$i] = chr(rand(0, 255));
        }

        $frameHead = array_merge($frameHead, $mask);
    }
    $frame = implode('', $frameHead);

    // append payload to frame:
    for ($i = 0; $i < $payloadLength; $i++) {
        $frame .= ($masked === true) ? $payload[$i] ^ $mask[$i % 4] : $payload[$i];
    }

    return $frame;
}

private function decode($data)
{
    $unmaskedPayload = '';
    $decodedData = array();

    // estimate frame type:
    $firstByteBinary = sprintf('%08b', ord($data[0]));
    $secondByteBinary = sprintf('%08b', ord($data[1]));
    $opcode = bindec(substr($firstByteBinary, 4, 4));
    $isMasked = ($secondByteBinary[0] == '1') ? true : false;
    $payloadLength = ord($data[1]) & 127;

    // unmasked frame is received:
    if (!$isMasked) {
        return array('type' => '', 'payload' => '', 'error' => 'protocol error (1002)');
    }

    switch ($opcode) {
        // text frame:
        case 1:
            $decodedData['type'] = 'text';
            break;

        case 2:
            $decodedData['type'] = 'binary';
            break;

        // connection close frame:
        case 8:
            $decodedData['type'] = 'close';
            break;

        // ping frame:
        case 9:
            $decodedData['type'] = 'ping';
            break;

        // pong frame:
        case 10:
            $decodedData['type'] = 'pong';
            break;

        default:
            return array('type' => '', 'payload' => '', 'error' => 'unknown opcode (1003)');
    }

    if ($payloadLength === 126) {
        $mask = substr($data, 4, 4);
        $payloadOffset = 8;
        $dataLength = bindec(sprintf('%08b', ord($data[2])) . sprintf('%08b', ord($data[3]))) + $payloadOffset;
    } elseif ($payloadLength === 127) {
        $mask = substr($data, 10, 4);
        $payloadOffset = 14;
        $tmp = '';
        for ($i = 0; $i < 8; $i++) {
            $tmp .= sprintf('%08b', ord($data[$i + 2]));
        }
        $dataLength = bindec($tmp) + $payloadOffset;
        unset($tmp);
    } else {
        $mask = substr($data, 2, 4);
        $payloadOffset = 6;
        $dataLength = $payloadLength + $payloadOffset;
    }

    /**
     * We have to check for large frames here. socket_recv cuts at 1024 bytes
     * so if websocket-frame is > 1024 bytes we have to wait until whole
     * data is transferd.
     */
    if (strlen($data) < $dataLength) {
        return false;
    }

    if ($isMasked) {
        for ($i = $payloadOffset; $i < $dataLength; $i++) {
            $j = $i - $payloadOffset;
            if (isset($data[$i])) {
                $unmaskedPayload .= $data[$i] ^ $mask[$j % 4];
            }
        }
        $decodedData['payload'] = $unmaskedPayload;
    } else {
        $payloadOffset = $payloadOffset - 4;
        $decodedData['payload'] = substr($data, $payloadOffset);
    }

    return $decodedData;
}

//пользовательские сценарии:

public function onOpen($connect_id, $data) {
  echo "onOpen";
  echo $connect_id."\r\n";
  print_r($data);
  $this->api->onMessage_event($data);
}

public function onClose($peername) {
  echo "================================ ws_server.onClose: start =========================\r\n";
  $data = $this->sessions[$peername];
  $this->api->onClose($data);
  unset($this->sessions['peername']);
  print_r($this->sessions);
}

public function onMessage($peername,$message) {
//  echo "================================ ws_server.onMessage: start =========================\r\n";
//  echo "peername:".$peername."\r\n";

  $data = $this->sessions[$peername];
  $msg = $this->decode($message)['payload'];
  $data['msg'] = $msg;

  $this->api->onMessage_event($data);
}


  public function Message_to_user_Send($peername, $msg) {
    $data_send = $this->encode($msg);
    $this->connections->stream_data_send($peername,$data_send);
  }

  public function Message_broadband_send($msg) {
    $keys = array_keys($this->sessions);
    $data_send = $this->encode($msg);
    foreach($keys as $peername) {
      $this->connections->stream_data_send($peername,$data_send);
    }
  }

private function http_parse_headers($raw_headers) {
     $headers = array(); 
     $key = ''; 

     foreach(explode("\n", $raw_headers) as $i => $h) { 
      $h = explode(':', $h, 2); 

      if (isset($h[1])) { 
       if (!isset($headers[$h[0]])) 
        $headers[$h[0]] = trim($h[1]); 
       elseif (is_array($headers[$h[0]])) { 
        $headers[$h[0]] = array_merge($headers[$h[0]], array(trim($h[1]))); 
       } 
       else { 
        $headers[$h[0]] = array_merge(array($headers[$h[0]]), array(trim($h[1]))); 
       } 

       $key = $h[0]; 
      } 
      else { 
       if (substr($h[0], 0, 1) == "\t") 
        $headers[$key] .= "\r\n\t".trim($h[0]); 
       elseif (!$key) 
        $headers[0] = trim($h[0]); 
      } 
     } 

     return $headers; 
    } 

private function get_parse($get) {
  $params = array();
  $get = substr($get,strpos($get,'?')+1);
  $get = substr($get,0,strpos($get,' '));
  foreach(explode("&",$get) as $param) {
    $p_arr = explode("=",$param);
    $params[$p_arr[0]] = $p_arr[1];
  }
  return $params;
}

  public function onConnect($connect_id) {
    $this->connections->data_read($connect_id);
  }

}