<?php
namespace ws_server;
if (!defined('index_constant')) die ('Not a valid entry point');
use \Exception;

class connections {
  private $socket_master=false;
  private $connections = array();
  private $server;

  function __construct($server) {
    $this->server = $server;
  }

  public function stream_add($stream,$peername) {
    $this->connections[$peername] = $stream;
  }

  public function stream_del($c) {
    fclose($c);
    $key = array_search($c, array_column($this->connections, 'stream'));
    unset($this->connections[$key]);
  }

  private function streams_get() {
    return $this->connections;
/*
    $streams = array();
    foreach ($this->connections as $connect) {
      $streams[] = $connect['stream'];
    }
    return $streams;
*/
  }


private function connection_new($res) {
}

  public function init() {
    $this->bindAddress = "tcp://0.0.0.0:8000";
    if (($this->socket_master = stream_socket_server($this->bindAddress, $errNo, $errString)) === false) {
      throw new \RuntimeException('Could not create listening socket: ' . $errString);
    }

    if (stream_set_blocking($this->socket_master, 0) === false) {
        throw new \RuntimeException("stream_set_blocking failed");
    }
}


  public function data_wait() {
    $read = $this->streams_get();
    $read[] = $this->socket_master;
    $write = $except = null;

    if (!stream_select($read, $write, $except, 0,20)) {
      return;
    }

    if (in_array($this->socket_master, $read)) { // new connection
        error_log("new connection");
        if ($connect = stream_socket_accept($this->socket_master, -1, $peername)) {
            $this->stream_add($connect,$peername);
            $this->onNewConnect($connect);
            $this->onOpen($connect);
        }
        unset($read[array_search($this->socket_master, $read)]);
    }


    foreach($read as $connect) {
//      echo "incoming";
      $data = $this->stream_read($connect);

      if (!$data) {
        // TODO: Возможно данные еще не подъехали: проверяем таймаут ожидания данных
        //$this->stream_onClose($connect);
        continue;
      }
      $this->onMessage($connect, $data);
    }
  }

  public function listen_stop() {
    fclose($server);
  }

  private function onNewConnect($stream) {
    $name = stream_socket_get_name($stream,true);
    $this->server->onNewConnect($name);
  }

  private function onOpen($stream) {
  }

  private function stream_onClose($stream) {
    echo "================== stream_onClose :start ================================\r\n";
    $peername = stream_socket_get_name($stream,true);
    echo $peername."\r\n";
    unset($this->connections[$peername]);
    $this->onClose($peername);
  }

  public function onClose($peername) {
    $this->server->onClose($peername);
  }

  private function onMessage($stream,$data) {
//    $data = $this->stream_read($stream);
    $peername = stream_socket_get_name($stream,true);
    $this->server->onMessage($peername,$data);
  }

  public function stream_by_name_read($peername) {
    echo "================== stream_by_name_read :start ================================\r\n";
    print_r($this->connections);
    $stream = $this->connections[$peername];
    print_r($stream);
    return $this->stream_read($stream);
  }

  public function stream_data_send($peername,$data) {
//    echo "================== stream_data_send :start ================================\r\n";
//    echo "peername:".$peername."\r\n";
    $stream = $this->connections[$peername];
    $this->data_send($stream,$data);
  }

  private function data_send($stream,$data) {
    fwrite($stream, $data);
  }

private function stream_read($stream) {
//  echo "===============stream_read:start================\r\n";
  stream_set_blocking($stream,0);
  $tmp_res = stream_set_timeout($stream,0,500);
  if (!$tmp_res) {
    echo "cant set timeout on stream\r\n";
    exit;
  }

    $in_data = '';
    $buf=' ';
    while (strlen($buf)>0) {
//      echo "read:".strlen($buf)."\r\n";
      $buf = fread($stream,8000);
      $in_data .= $buf;
    }
  return $in_data;
}


}