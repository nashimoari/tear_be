<?php
namespace ws_server;

abstract class cAPI_abstract {

  abstract public function onMessage_event($data);
  abstract public function ws_server_set($ws_server);
  abstract public function news_check();
  abstract public function onClose($data);
}