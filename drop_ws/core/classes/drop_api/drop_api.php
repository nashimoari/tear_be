<?php
namespace drop_api;

class drop_api extends \ws_server\cAPI_abstract {
  private $ws_server;
  private $registry;


  public function registry_set($registry) {
    $this->registry = $registry;
  }


  public function onMessage_event($data) {
//    echo "=============API: onMessage_event================\r\n";
//    echo $data."\r\n";
    $arr = json_decode($data['msg'],true);

    if (strpos($arr['msg_type'],'_request')=== false) {
      print_r($arr);
      return;
    }

    $game_zone = $this->registry->get('game_zone');
    $tmp_res = $game_zone->message_request($arr,$data);
    if (isset($tmp_res['response_messages'])) {
      foreach ($tmp_res['response_messages'] as $msg_send) {
        $this->ws_server->Message_to_user_Send($data['session']['peername'],json_encode($msg_send));
      }
    }

  }

  public function ws_server_set($ws_server) {
    $this->ws_server = $ws_server;
  }

  public function news_check() {
    $game_zone = $this->registry->get('game_zone');
    $tmp_res = $game_zone->news_check();
    if (isset($tmp_res['response_messages'])) {
      foreach ($tmp_res['response_messages'] as $msg_send) {
        $this->ws_server->Message_to_user_Send($msg_send['peername'],json_encode($msg_send['message']));
      }
    }
  }

  public function onClose($data) {
    $game_zone = $this->registry->get('game_zone');
    $tmp_res = $game_zone->gamer_leave($data['session']['peername']);
  }

}