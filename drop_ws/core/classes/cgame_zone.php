<?php

class cgame_zone {
  private $drops_cnt;
  private $time_drop_last_spawn;
  private $lil_drops;
  private $drops_arr = [];
  private $drops_colors_arr = ['#E6E6FA','#FFE4E1','#87CEEB','#40E0D0','#006400','#2E8B57','#CD5C5C','#BC8F8F','#CD5C5C'];
  private $gamers = [];
  private $session_info;
  function __construct() {
    $this->drops_cnt = 1000;
    for ($i=0;$i<$this->drops_cnt;$i++) {
        $this->drop_spawn();
    }
  }


  public function message_request($msg,$session_info) {
//    echo "=========== GAME_ZONE : ".__FUNCTION__." ========== \r\n";
//    print_r($session_info);

    $this->session_info = $session_info;
    $f_name = 'api_'.$msg['msg_type'];
    $tmp_res = $this->$f_name($msg);
    return $tmp_res;
  }

private function api_canvas_resize_request($msg) {
    if (!isset($this->gamers[$this->session_info['session']['peername']])) {
        return;
    }
    $gamer = $this->gamers[$this->session_info['session']['peername']];
    $tmp_res = $gamer->canvas_set($msg);
}
  private function api_gamer_movement_request($msg) {
    if (!isset($this->gamers[$this->session_info['session']['peername']])) {
        return;
    }

    $resp_arr = [];
    $resp_arr['msg_type'] = 'state_update';

//    echo "=========== GAME_ZONE : ".__FUNCTION__." ========== \r\n";

    // get gamer id
    $gamer = $this->gamers[$this->session_info['session']['peername']];
    $tmp_res = $gamer->move_request($msg);
/*    if ($tmp_res == 1) {
      $resp_arr['data']['gamer_state'] = $gamer->state_get();
      return ['response_messages' => [$resp_arr] ];
    }
  */
  }


  private function api_gamer_init_request($msg) {
//    echo "=========== GAME_ZONE : ". __FUNCTION__." ========== \r\n";

    // add gamer
    $gamer = new \cgamer();
    $gamer->init_request($msg);
    $this->gamers[$this->session_info['session']['peername']] = $gamer;
  }

  public function news_check() {
      $this->drop_respawn();
      $resp_arr = [];

      
    // move gamers
    foreach ($this->gamers as $peername => $gamer) {
      $tmp_res = $gamer->move();
      if ($tmp_res == 1) {
        $this->tears_intersection_check($gamer);
        $resp_tmp = [];
        $resp_tmp['peername'] = $peername;
        $resp_tmp['message']['msg_type'] = 'state_update';
        $resp_tmp['message']['data']['gamer_state'] = $gamer->state_get();
        $resp_tmp['message']['data']['tears_arr'] = $this->tears_get($gamer);
        $resp_tmp['message']['data']['gamers_arr'] = $this->gamers_get($peername,$gamer);
        $resp_arr[] = $resp_tmp;
      }    
    }
    return ['response_messages' => $resp_arr];
  }
  
  public function gamer_leave($peername) {
      unset ($this->gamers[$peername]);
  }
  
  
  private function tears_intersection_check($gamer) {
    $gamer_state =  $gamer->state_get();
    $start_x = $gamer_state['x'] - $gamer_state['size'];
    $end_x = $gamer_state['x'] + $gamer_state['size'];
    $start_y = $gamer_state['y'] - $gamer_state['size'];
    $end_y = $gamer_state['y'] + $gamer_state['size'];
    $distance = -1;

    foreach ($this->drops_arr as $i => $drop) {
      if (($drop[0]>$start_x)&&($drop[0]<$end_x)&&($drop[1]>$start_y)&&($drop[1]<$end_y)) {
        $distance = sqrt(pow(($gamer_state['x']-$drop[0]), 2) + pow(($gamer_state['y']-$drop[1]), 2));
        if ($distance<($gamer_state['size'])) {
            $this->tear_eat($i,$gamer);
        }
      }
    }
  }


    private function tear_eat($i,$gamer) {
        unset($this->drops_arr[$i]);
        $gamer->size_change(0.1);
    }
  

    private function tears_get($gamer) {
        $tears_arr = [];
        $gamer_state =  $gamer->state_get();
        $canvas = $gamer->canvas_get();
        $zoom = $gamer->zoom_get();
        $w = $canvas['w'];
        $h = $canvas['h'];
        $w_virtual = $w/$zoom;
        $h_virtual = $h/$zoom;
        $x_virtual_start = $gamer_state['x'] - $w_virtual/2;
        $x_virtual_end = $gamer_state['x'] + $w_virtual/2;
        $y_virtual_start = $gamer_state['y'] - $h_virtual/2;
        $y_virtual_end = $gamer_state['y'] + $h_virtual/2;

        foreach ($this->drops_arr as $drop) {
          if (($drop[0]>$x_virtual_start)&&($drop[0]<$x_virtual_end)&&($drop[1]>$y_virtual_start)&&($drop[1]<$y_virtual_end)) {
            $tears_arr[] = $drop;
          }
        }
        return $tears_arr;
    }
    
    private function gamers_get($peername,$gamer) {
        $gamers_arr = [];
        $gamer_state =  $gamer->state_get();
        $canvas = $gamer->canvas_get();
        $zoom = $gamer->zoom_get();
        $w = $canvas['w'];
        $h = $canvas['h'];
        $w_virtual = $w/$zoom;
        $h_virtual = $h/$zoom;
        $x_virtual_start = $gamer_state['x'] - $w_virtual/2;
        $x_virtual_end = $gamer_state['x'] + $w_virtual/2;
        $y_virtual_start = $gamer_state['y'] - $h_virtual/2;
        $y_virtual_end = $gamer_state['y'] + $h_virtual/2;


        $gamers_state_arr = [];
      
        foreach ($this->gamers as $item_peername => $item_gamer) {
            if ($item_peername != $peername) {
                $gamer_state_tmp = $item_gamer->state_get();                
                if (($gamer_state_tmp['x']>$x_virtual_start)&&($gamer_state_tmp['x']<$x_virtual_end)&&($gamer_state_tmp['y']>$y_virtual_start)&&($gamer_state_tmp['y']<$y_virtual_end)) {
                    $gamers_state_arr[] = $gamer_state_tmp;
                }
                
            }
        }
        return $gamers_state_arr;
    }

    private function drop_spawn() {
      $x = rand(1,1999);
      $y = rand(1,1999);
      $color = rand(0,sizeof($this->drops_colors_arr)-1);
      //$this->drops_arr[] = array($x,$y,$this->drops_colors_arr[$color]);
      $this->drops_arr[] = array($x,$y,$color);
      $this->time_drop_last_spawn = time();
    }
    
    private function drop_respawn() {
        if ((sizeof($this->drops_arr) < $this->drops_cnt)&&($this->time_drop_last_spawn < time())) {
            $this->drop_spawn();
        }
    }
    
}

