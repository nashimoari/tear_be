<?php


class cgamer {
    private $canvas = ['w'=>300, 'h'=>300];
    private $name;
    private $x;
    private $y;
    private $size;
    private $last_sended_pos = ['x'=>-1, 'y'=>-1];
    
    private $x_sign = 1;
    private $y_sign = 1;
    private $x_coef = 1;
    
    private $speed_pps = 100; // pixels per second
    private $time_last_move; // last time when was updated position
    private $zoom = 1; // zoom level

  public function init_request($data) {
//    echo "=========== ".get_class()." : ". __FUNCTION__." ========== \r\n";

    $this->size = 30;
    $this->x = rand(50, 1500);
    $this->y = rand(50,1500);
    $this->name = $data['gamer_name'];
    $this->time_last_move = microtime(true);
    return 1;
  }

  public function move_request($data) {
//    echo "=========== ".get_class()." : ". __FUNCTION__." ========== \r\n";
    $this->x_sign = $data['x_sign'];
    $this->y_sign = $data['y_sign'];
    $this->x_coef = $data['x_coef'];
    
    $this->time_last_move = microtime(true);
  }

  public function size_change($val) {
      $this->size += $val;
  }

  public function division() {
  }

  public function state_get() {
    return ['x'=>$this->x, 'y'=>$this->y, 'size'=>$this->size];
  }
  
  public function move() {
    $ret_value = 0;
    
    // calculate distance
    $time_diff = microtime(true) - $this->time_last_move;
    
    $sign_x = $this->x_sign <=> 0;
    $sign_y = $this->y_sign <=> 0;
    
    $y_vector = $this->speed_pps/(1+$this->x_coef);
    $x_vector = $this->speed_pps - $y_vector;
    if ($sign_x!=0) {
       $this->x+=$sign_x*$time_diff*$x_vector;
    }

    if ($sign_y!=0) {    
      $this->y+=$sign_y*$time_diff*$y_vector;
    }

    if ($this->x <2 ) { $this->x = 2; };
    if ($this->x >1998 ) { $this->x = 1998; };

    if ($this->y <2 ) { $this->y = 2; };
    if ($this->y >1998 ) { $this->y = 1998; };

//    echo "X:".$this->x.";Y:".$this->y."\r\n";
    if ($this->last_sended_pos['x'] <> floor($this->x)) {
      $this->last_sended_pos['x'] = floor($this->x);
      $ret_value = 1;
    }

    if ($this->last_sended_pos['y'] <> floor($this->y)) {
      $this->last_sended_pos['y'] = floor($this->y);
      $ret_value = 1;
    }

    $this->time_last_move = microtime(true);
    return $ret_value;      
  }


  public function canvas_set($data) {
    $this->canvas['w']  = $data['width'];
    $this->canvas['h'] = $data['height'];
  }
  
  public function canvas_get() {
      return ['w'=>$this->canvas['w'], 'h'=>$this->canvas['h']];
  }

  private function zoom_recalc() {
      
  }
  
  public function zoom_get() {
      return $this->zoom;
  }
  
}